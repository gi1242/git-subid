# git filter to substitute commit ID for `$Id$`

This filter replaces lines containing `$Id$` or `$Id: ... $` with `$Id: xxx :`,
where `xxx` is the full commit hash. The main reason I wrote this filter is to
help sharing files with co-authors who don't use git. I can email them a file
with the commit hash at the bottom. When I get their modifications back by
email, I can merge their changes back easily using:

    $ git checkout -b author-b1 <hash at bottom of file>
    # save their changes in the local repository
    $ git commit --author ... -a
    $ git checkout master
    $ git merge author-b1
    # Email the file back to them

This avoids confusion in case an author edits an older version, etc. (Ideally
all co-authors should use git, but I don't think that will ever happen.)

## Usage

1. Edit `.gitattributes`, `.git/info/attributes` or `~/.config/git/attributes`
   and add `filter=subid` for all file types you want substituted:

        *.tex filter=subid
        *.bib filter=subid

2. Edit `.git/config` or `~/.gitconfig` and define the filter `subid`:

        [filter "subid"]
            clean = /path/to/git-subid/clean.sh
            smudge = /path/to/git-subid/smudge.sh

3. Put a line containing `$Id$` in your file. It will be replaced by the
   commit ID the current head whenever the file is checked out.

## Updating `$Id$` after check-ins

The `$Id$` is only updated on check outs **NOT CHECKINS**. Thus if you
`git pull`, then the `$Id$` in all files *modified* by that commit will be
updated, but everything else will be untouched. Also, if you `git commit`, the
`$Id$` token in files in your local repository will be unchanged. If you want
to update the `$Id$` token in all files in your local repository, then you
have to re-checkout the file.

You can re-checkout everything manually using

    git checkout HEAD -- "$(git rev-parse --show-toplevel)"

or use the `git-recheckout.sh` script. This is a bit safer as it stashes your
modifications first. A convenient way to run it is to set up a git-alias:

    git config --set alias.re-checkout '! /path/to/git-subid/git-recheckout.sh'

## Keeping `$Id$` automatically updated.

If you want `$Id$` to be *automatically* updated after commits, pulls, etc.
then use the following steps.

1. Run `git-recheckout.sh` from your `post-commit` and `post-merge` hooks.
   If you don't have these hooks, simply symlink them to `git-recheckout.sh`

2. Install `git-subid/post-checkout` as your post-checkout hook. If you don't
   have one already just symlink to it.

For convenience this repository contains `post-commit` and `post-merge` files
that simply symlink to `git-recheckout.sh`. Thus, if you have no special hooks
in your repository, you can simply do

    cd -P .git/hooks/
    ln -s /path/to/git-subid/post-* .
