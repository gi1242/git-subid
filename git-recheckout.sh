#! /bin/sh
# Created   : Fri 20 May 2016 11:24:43 PM EDT
# Modified  : Sat 21 May 2016 02:04:50 PM EDT
# Author    : GI <a@b.c>, where a='gi1242+sh', b='gmail', c='com'

# Checkout all files in the repo, so that the smudge filter is re-run.

# Avoid infinite loop
[ -n "$SUBID_RUNNING_RECHECKOUT" ] && exit
export SUBID_RUNNING_RECHECKOUT=1

QUIET=1

if ! git diff-index --quiet HEAD; then
    git stash save ${QUIET:+-q}
    stashed=1
else
    stashed=
fi

git checkout HEAD -- "$(git rev-parse --show-toplevel)"

[ -n "$stashed" ] && git stash pop ${QUIET:+-q}
